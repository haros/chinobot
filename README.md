**ChinoBot** is a simple Discord chat-bot written in Java using [JDA](https://github.com/DV8FromTheWorld/JDA) (a Java wrapper for the Discord API).

It currently has 4 commands:
*  Ping
*  Echo
*  ChangeNickname
*  SendWaifu

*Ping* - !ping <br>
ChinoBot responds with Pong and response time in ms.

*Echo* - !echo I am a bot <br>
ChinoBot repeats what you type, in this case it'll respond "I am a bot"

*ChangeNickname* - !nick @UserMention Gremlin <br>
ChinoBot changes @UserMention's nickname to Gremlin.

*SendWaifu* - !waifu / !waifu 6 / !waifu dipsy<br>
ChinoBot sends a random img-url waifu in chat. The image-urls are defined in <br>
the config.json file in an array, and that array is put into an ArrayList. <br>
You can get an image by index and by string.


Work in progress <br>
* [ ]  *Delete* - will delete messages


This bot is mostly just me practicing Java.