package me.kleopi.chinobot.commands;

import me.kleopi.chinobot.eventhandlers.Command;
import me.kleopi.chinobot.eventhandlers.CommandEvent;
import me.kleopi.chinobot.eventhandlers.MessageParser;
import me.kleopi.chinobot.services.WaifuOwnership;
import me.kleopi.chinobot.utility.ParsingUtility;
import net.dv8tion.jda.api.entities.MessageChannel;

import java.util.*;

public class SendWaifu extends Command {

    public String getName() {
        return "waifu";
    }
    public String getRolePermission() {
        return null;
    }

    public void execute(CommandEvent event, MessageParser parser) {
        MessageChannel channel = event.getMessage().getChannel();

        // overview links-indexes, and waifuOwners' name, message and list of link-indexes
        List<String> links = event.getWaifuConfiguration().getWaifuImageLinks();
        List<WaifuOwnership> waifuOwnerships = event.getWaifuConfiguration().getWaifuOwnerships();


        // variables to help with the logic of checking input and responding accordingly
        int index = 0;
        boolean foundWaifu = false;

        if (links.size() == 0) {
            channel.sendMessage("Waifus not found, check your terminal for more information.").queue();
            System.out.println("There are no img-links under waifuImageLinks in your config-file. Format: " +
                    "\"waifuImageLinks\": [link1, link2, link3, ...]");
            return;

        }

        // finds out if there's any potential indexes/name-strings in messageContent
        if (parser.getMessageContent() == null) {
            index = new Random().nextInt(links.size());
        }
        // use int for index if it is in range of links' indexes
        else if (ParsingUtility.isInt(parser.getMessageContent())) {
            index = Integer.parseInt(parser.getMessageContent());

            // if index isn't in range then get random index
            if (index < 0 || index > links.size() - 1) {
                channel.sendMessage("You've tried to summon a waifu that doesn't exist! " +
                        "Try a number from (0 to " + (links.size() - 1) + ") instead.").queue();
                return;
            }
        }
        // otherwise, index will be a string, match string to existing name-strings
        else {
            String waifuOwnerName = parser.getMessageContent().toLowerCase();

            for (WaifuOwnership waifuOwnership : waifuOwnerships) {
                if (waifuOwnership.getName().equals(waifuOwnerName)) {
                    List<Integer> waifus = waifuOwnership.getWaifus();

                    // get a random index in list of waifus belonging to waifuOwner
                    index = waifus.get(new Random().nextInt(waifus.size()));
                    foundWaifu = true;
                }
            }
            if (!foundWaifu) {
                channel.sendMessage("You've tried to summon a waifu that doesn't exist! " +
                        "Try a number from (0 to " + links.size() + ") instead.").queue();
                return;
            }
        }

        // if index is in waifus, post personalised message
        for (WaifuOwnership waifuOwnership : waifuOwnerships) {
            for (int value : waifuOwnership.getWaifus()) {
                if (value == index) {
                    channel.sendMessage(waifuOwnership.getMessage() + links.get(index)).queue();
                    foundWaifu = true;
                }
            }
        }

        if (!foundWaifu) channel.sendMessage("Here's your waifu: " + links.get(index)).queue();
        channel.sendMessage("*Don't forget to check out <https://waifulabs.com>*").queue();
    }
}
