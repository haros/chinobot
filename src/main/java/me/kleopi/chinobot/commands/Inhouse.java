package me.kleopi.chinobot.commands;

import me.kleopi.chinobot.eventhandlers.Command;
import me.kleopi.chinobot.eventhandlers.CommandEvent;
import me.kleopi.chinobot.eventhandlers.MessageParser;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;

import java.util.*;

public class Inhouse extends Command {

    @Override
    public String getName() {
        return "inhouse";
    }

    @Override
    public String getRolePermission() {
        return null;
    }

    @Override
    public void execute(CommandEvent event, MessageParser parser) {
        Message message = event.getMessage();
        Guild guild = message.getGuild();

        if (event.getRoleConfig().getInhouseRole() == null) {
            message.getChannel().sendMessage("No inhouse role set.").queue();
            return;
        }

        Role joined = guild.getRoleById(event.getRoleConfig().getInhouseRole());

        List<String> possibleFirstArgs = new ArrayList<String>(Arrays.asList("join", "leave", "clear", "count"));

        try {
            String firstArgument = parser.getMessageContent().split(" ")[0];
        } catch (Exception e){
            message.getChannel().sendMessage(expectedArguments(possibleFirstArgs + " (user-mention, user-mention, ...)")).queue();
            return;
        }

        String firstArgument = parser.getMessageContent().split(" ")[0];

        if (!possibleFirstArgs.contains(firstArgument) && firstArgument != null) {
            message.getChannel().sendMessage(expectedArguments(possibleFirstArgs + " (user-mention, user-mention, ...)")).queue();
            return;
        }

        String teamMembers = "";

        if (joined == null) {
            return;
        }

        Map<String, String> emojis = new HashMap<>();
        emojis.put("kiss", "<a:kiss:646033327559540747>");
        emojis.put("tpLove", "<:tpLove:522527667740868608>");
        emojis.put("peepoevil", "<:peepoevil:648116180883603476>");

        switch (firstArgument) {
            case "join":
                if (message.getMentionedUsers().size() > 0) {
                    for (Member member : message.getMentionedMembers()) {
                        guild.addRoleToMember(member, joined).queue();
                        teamMembers = teamMembers.concat("\n" + member.getEffectiveName() + " : added " + emojis.get("tpLove"));
                    }
                } else if (message.getMember() != null) {
                    guild.addRoleToMember(message.getMember(), joined).queue();
                    teamMembers = teamMembers.concat("\n" + message.getMember().getEffectiveName() + " : added " + emojis.get("kiss"));
                }
                message.getChannel().sendMessage(teamMembers).queue();
                break;
            case "leave": {
                if (message.getMentionedUsers().size() > 0) {
                    for (Member member : message.getMentionedMembers()) {
                        guild.removeRoleFromMember(member, joined).queue();
                        teamMembers = teamMembers.concat("\n" + member.getEffectiveName() + " : removed " + emojis.get("tpLove"));
                    }
                } else if (message.getMember() != null) {
                    guild.removeRoleFromMember(message.getMember(), joined).queue();
                    teamMembers = teamMembers.concat("\n" + message.getMember().getEffectiveName() + " : removed " + emojis.get("kiss"));
                }
                message.getChannel().sendMessage(teamMembers).queue();
                break;
            }
            case "clear":
                for (Member member : guild.getMembersWithRoles(joined)) {
                    guild.removeRoleFromMember(member.getId(), joined).queue();
                }
                message.getChannel().sendMessage("Cleared inhouse roles! " + emojis.get("peepoevil")).queue();
                break;
            case "count":
                for (Member member : guild.getMembersWithRoles(joined)) {
                    teamMembers = teamMembers.concat("\n" + member.getEffectiveName() );
                }
                message.getChannel().sendMessage(teamMembers + "\nPeople joining inhouse: " + guild.getMembersWithRoles(joined).size() +  "/10").queue();
        }
    }
}
