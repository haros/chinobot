package me.kleopi.chinobot.commands;

import me.kleopi.chinobot.eventhandlers.Command;
import me.kleopi.chinobot.eventhandlers.CommandEvent;
import me.kleopi.chinobot.eventhandlers.MessageParser;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

import java.util.Objects;

public class ChangeNickname extends Command {

    private String expectedArguments = expectedArguments("[@user-mention] \"new nick\"");

    public String getName() {
        return "nick";
    }
    public String getRolePermission() {
        return null;
    }

    @Override
    public void execute(CommandEvent event, MessageParser parser) {

        Message message = event.getMessage();

        if (parser.getMessageContent() == null || parser.getMessageContentWithoutSecondArg() == null) {
            message.getChannel().sendMessage(expectedArguments).queue();
            return;
        }

        String userId = message.getMentionedMembers().get(0).getId();
        String newNick = parser.getMessageContentWithoutSecondArg();

        User user = message.getJDA().getUserById(userId);
        assert user != null;
        Objects.requireNonNull(message.getGuild().getMember(user)).modifyNickname(newNick).queue();
        message.getChannel().sendMessage("Changed " + user.getName() + "'s nickname to" + newNick + " for you, " +
                Objects.requireNonNull(message.getJDA().getUserById(message.getAuthor().getId())).getAsMention()).queue();

    }
}
