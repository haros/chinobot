package me.kleopi.chinobot.commands;

import me.kleopi.chinobot.eventhandlers.Command;
import me.kleopi.chinobot.eventhandlers.CommandEvent;
import me.kleopi.chinobot.eventhandlers.MessageParser;

public class Ping extends Command {
    private String expectedArguments = expectedArguments("none");

    public String getName() {
        return "ping";
    }
    public String getRolePermission() {
        return null;
    }

    public void execute(CommandEvent event, MessageParser parser) {
        event.getMessage().getChannel().sendMessage(
                "Pong! (Responded in " + event.getMessage().getJDA().getGatewayPing() + " ms)").queue();
    }
}