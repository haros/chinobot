package me.kleopi.chinobot.commands;

import me.kleopi.chinobot.eventhandlers.Command;
import me.kleopi.chinobot.eventhandlers.CommandEvent;
import me.kleopi.chinobot.eventhandlers.MessageParser;
import net.dv8tion.jda.api.entities.Message;

public class Echo extends Command {

    private String expectedArguments = expectedArguments("\"Your text here.\"");

    public String getName() {
        return "echo";
    }
    public String getRolePermission() {
        return "mod";
    }

    public void execute(CommandEvent event, MessageParser parser) {
        Message message = event.getMessage();

        if (parser.getMessageContent() == null) {
            message.getChannel().sendMessage(expectedArguments).queue();
            return;
        }
        message.getChannel().sendMessage(parser.getMessageContent()).queue();
    }
}