package me.kleopi.chinobot.commands;

import me.kleopi.chinobot.eventhandlers.Command;
import me.kleopi.chinobot.eventhandlers.CommandEvent;
import me.kleopi.chinobot.eventhandlers.MessageParser;

public class ListAssignableRoles extends Command {

    public String getName() {
        return "roles";
    }
    public String getRolePermission() {
        return null;
    }

    @Override
    public void execute(CommandEvent event, MessageParser parser) {

        StringBuilder roles = new StringBuilder("**Roles available:**\n");

        for (String role : event.getRoleConfig().getAssignableRoles().keySet()) {
            roles.append("- ").append(role).append("\n");
        }

        if(event.getRoleConfig().getAssignableRoles().isEmpty()) {
            event.getMessage().getChannel().sendMessage("No self-assignable roles.").queue();
        } else {
            event.getMessage().getChannel().sendMessage(roles.toString()).queue();
        }
    }
}
