package me.kleopi.chinobot.commands;

import me.kleopi.chinobot.eventhandlers.Command;
import me.kleopi.chinobot.eventhandlers.CommandEvent;
import me.kleopi.chinobot.eventhandlers.MessageParser;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;

import java.util.HashMap;
import java.util.Map;

public class RoleAssignment extends Command {

    private final String expectedArguments = expectedArguments("\"role-name\"");

    public String getName() {
        return "role";
    }

    public String getRolePermission() {
        return null;
    }

    public void execute(CommandEvent event, MessageParser parser) {

        Message message = event.getMessage();
        Guild guild = message.getGuild();

        Map<String, Role> assignableRoles = new HashMap<>();

        Map<String, String> configRoles = new HashMap<>(event.getRoleConfig().getAssignableRoles());

        for (String key : configRoles.keySet()) {
            assignableRoles.put(key, guild.getRoleById(configRoles.get(key)));
        }

        if (parser.getMessageContent() == null) {
            message.getChannel().sendMessage(expectedArguments).queue();
            return;
        } else if (!assignableRoles.containsKey(parser.getMessageContent())) {
            message.getChannel().sendMessage("That's not an assignable role. To see assignable roles do !roles").queue();
            return;
        }

        Role role = assignableRoles.get(parser.getMessageContent());

        if (message.getMember() != null) {
            if (message.getMember().getRoles().contains(role)) {
                guild.removeRoleFromMember(message.getMember(), role).queue();
                message.getChannel().sendMessage(role.getName() + " has been removed.").queue();
            } else {
                guild.addRoleToMember(message.getMember(), role).queue();
                message.getChannel().sendMessage(role.getName() + " has been added.").queue();
            }
        }
    }
}
