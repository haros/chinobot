package me.kleopi.chinobot.commands;

import me.kleopi.chinobot.eventhandlers.Command;
import me.kleopi.chinobot.eventhandlers.CommandEvent;
import me.kleopi.chinobot.eventhandlers.MessageParser;
import me.kleopi.chinobot.utility.ParsingUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

public class DeleteMessages extends Command {

    public String getName() {
        return "delete";
    }
    public String getRolePermission() {
        return "mod";
    }

    public void execute(CommandEvent event, MessageParser parser) {
        final String expectedArgs = "Expected arguments: " + event.getConfiguration().getPrefix() + "delete [number of messages] \n*Note: Max 99*";


        Message message = event.getMessage();
        TextChannel channel = message.getTextChannel();

        String deleteArg = parser.getMessageContent();

        if (deleteArg == null || !ParsingUtility.isInt(deleteArg)) {
            channel.sendMessage(expectedArgs).queue();
            return;
        }
        int deleteAmount = Integer.parseInt(deleteArg) + 1;

        channel.getHistory().retrievePast(deleteAmount).queue(messages ->
                {
                    channel.purgeMessages(messages);
                    channel.sendMessage("Done deleting " + deleteArg + " messages.").queue();
                }
        );
    }
}
