package me.kleopi.chinobot.eventhandlers;

import me.kleopi.chinobot.services.BotConfiguration;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;

import java.util.ArrayList;
import java.util.List;

public class MessageParser {
    private int prefixOccurrences;
    private String command;
    private String messageContent = null;
    private String messageContentWithoutSecondArg = null;
    private List<String> userRoles = new ArrayList<>();

    public MessageParser(BotConfiguration config, Message message) {

        String prefix = config.getPrefix();

        if (message.getMember() != null) {
            for (Role role : message.getMember().getRoles()) {
                userRoles.add(role.getName().toLowerCase());
            }
        }
        // get raw message content and split up the message in an array
        String messageContentDisplay = message.getContentRaw();
        String[] args = messageContentDisplay.split(" ", 0);

        String firstArg = args[0];
        String[] divideFirstArg = firstArg.split(prefix);

        command = divideFirstArg[divideFirstArg.length - 1];
        prefixOccurrences = (divideFirstArg.length - 1);

        if (args.length > 1)
            messageContent = messageContentDisplay.substring(firstArg.length() + 1);
        if (args.length > 2)
            messageContentWithoutSecondArg = messageContent.substring(args[1].length());
    }

    public int getPrefixOccurrences() {
        return prefixOccurrences;
    }

    public String getCommand() {
        return command;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public String getMessageContentWithoutSecondArg() {
        return messageContentWithoutSecondArg;
    }

    public List<String> getUserRoles() {
        return userRoles;
    }
}