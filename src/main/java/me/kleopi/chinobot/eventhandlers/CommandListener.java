package me.kleopi.chinobot.eventhandlers;

import me.kleopi.chinobot.commands.*;
import me.kleopi.chinobot.services.BotConfiguration;
import me.kleopi.chinobot.services.Configuration;
import me.kleopi.chinobot.services.RoleConfiguration;
import me.kleopi.chinobot.services.WaifuConfiguration;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.ArrayList;

public class CommandListener extends ListenerAdapter {
    private static final ArrayList<Command> commands = new ArrayList<>();

    // the commands the bot listens for
    static {
        commands.add(new Ping());
        commands.add(new Echo());
        commands.add(new DeleteMessages());
        commands.add(new ChangeNickname());
        commands.add(new SendWaifu());
        commands.add(new RoleAssignment());
        commands.add(new ListAssignableRoles());
        commands.add(new Inhouse());
    }

    private final BotConfiguration config;
    private final RoleConfiguration roleConfig;
    private final WaifuConfiguration waifuConfig;


    public CommandListener(BotConfiguration config, RoleConfiguration roleConfig, WaifuConfiguration waifuConfig) {
        this.config = config;
        this.roleConfig = roleConfig;
        this.waifuConfig = waifuConfig;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        Message message = event.getMessage();
        MessageParser parser = new MessageParser(config, message);

        if (message.getAuthor().isBot()) {
            return;
        }

        for (Command command : commands) {
            if (command.getName().equalsIgnoreCase(parser.getCommand())) {
                if (parser.getPrefixOccurrences() > Math.max(config.getPrefixAmount(), config.getDeleteMessagePrefixAmount()) ||
                        parser.getPrefixOccurrences() < Math.min(config.getPrefixAmount(), config.getDeleteMessagePrefixAmount())) {
                    return;
                }

                if (parser.getPrefixOccurrences() == config.getDeleteMessagePrefixAmount()) {
                    command.delete(message);
                }
                if(parser.getUserRoles().contains(command.getRolePermission()) || command.getRolePermission() == null) {
                    command.execute(new CommandEvent(message, config, roleConfig, waifuConfig), parser);
                } else {
                    event.getMessage().getChannel().sendMessage("This command requires " + command.getRolePermission()).queue();
                }

            }
        }
    }
}
