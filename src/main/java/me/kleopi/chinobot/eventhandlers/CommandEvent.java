package me.kleopi.chinobot.eventhandlers;

import me.kleopi.chinobot.services.BotConfiguration;
import me.kleopi.chinobot.services.RoleConfiguration;
import me.kleopi.chinobot.services.WaifuConfiguration;
import net.dv8tion.jda.api.entities.Message;

public class CommandEvent {
    private final Message message;
    private final BotConfiguration botConfiguration;
    private final RoleConfiguration roleConfiguration;
    private final WaifuConfiguration waifuConfiguration;

    public CommandEvent(Message message, BotConfiguration botConfiguration, RoleConfiguration roleConfiguration, WaifuConfiguration waifuConfiguration) {
        this.message = message;
        this.botConfiguration = botConfiguration;
        this.roleConfiguration = roleConfiguration;
        this.waifuConfiguration = waifuConfiguration;
    }

    public Message getMessage() {
        return message;
    }

    public BotConfiguration getConfiguration() {
        return botConfiguration;
    }

    public RoleConfiguration getRoleConfig() {
        return roleConfiguration;
    }

    public WaifuConfiguration getWaifuConfiguration() {
        return waifuConfiguration;
    }
}
