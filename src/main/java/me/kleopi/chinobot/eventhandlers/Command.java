package me.kleopi.chinobot.eventhandlers;

import net.dv8tion.jda.api.entities.Message;

public abstract class Command {
    public abstract String getName();
    public abstract String getRolePermission();

    protected String expectedArguments(String arguments) {
        return "That doesn't work you dummy. Try using the proper command structure.``` !" + getName() + " " + arguments + "```";
    }

    public abstract void execute(CommandEvent event, MessageParser parser);

    public void delete(Message message) {
        message.delete().queue();
    }
}
