package me.kleopi.chinobot.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class Configuration {
    protected final static String path = "config.json";
    protected final static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    Configuration() {}

    protected static Configuration load() throws IOException {
        return gson.fromJson(loadString(), Configuration.class);
    }

    public void save(String path) throws IOException {
        final String jsonText = gson.toJson(this);
        Files.write(Paths.get(path), jsonText.getBytes());
    }

    protected static String loadString() throws IOException {
        File file = new File(path);
        if (!file.exists()) {
            return null;
        }
        //load it
        return new String(Files.readAllBytes(Paths.get(path)));
    }

}
