package me.kleopi.chinobot.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WaifuConfiguration extends Configuration {

    private final List<WaifuOwnership> waifuOwnerships;
    private final List<String> waifuImageLinks;

    public static WaifuConfiguration load() throws IOException {
        return gson.fromJson(loadString(), WaifuConfiguration.class);
    }

    public WaifuConfiguration(List<WaifuOwnership> waifuOwnerships, List<String> waifuImageLinks) {
        this.waifuOwnerships = waifuOwnerships;
        this.waifuImageLinks = waifuImageLinks;
    }

    private WaifuConfiguration() {
        this.waifuOwnerships = new ArrayList<>();
        this.waifuImageLinks = new ArrayList<>();
    }

    public List<WaifuOwnership> getWaifuOwnerships() {
        return waifuOwnerships;
    }

    public List<String> getWaifuImageLinks() {
        return waifuImageLinks;
    }
}
