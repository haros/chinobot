package me.kleopi.chinobot.services;

import java.io.IOException;

public class BotConfiguration extends Configuration {

    private final String token;
    private final String prefix;
    private final int prefixAmount;
    private final int deleteMessagePrefixAmount;

    public static BotConfiguration load() throws IOException {
        return gson.fromJson(loadString(), BotConfiguration.class);
    }

    public BotConfiguration(String token, String prefix, int prefixAmount, int deleteMessagePrefixAmount) {
        this.token = token;
        this.prefix = prefix;
        this.prefixAmount = prefixAmount;
        this.deleteMessagePrefixAmount = deleteMessagePrefixAmount;
    }

    private BotConfiguration() {
        this.token = null;
        this.prefix = null;
        this.prefixAmount = 1;
        this.deleteMessagePrefixAmount = 2;
    }
    
    public String getToken() {
        return token;
    }

    public String getPrefix() {
        return prefix;
    }

    public int getPrefixAmount() {
        return prefixAmount;
    }

    public int getDeleteMessagePrefixAmount() {
        return deleteMessagePrefixAmount;
    }

}