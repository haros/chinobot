package me.kleopi.chinobot.services;

import java.util.List;

public class WaifuOwnership {

    private String name;
    private String message;
    private List<Integer> waifus;

    public WaifuOwnership(String name, String message, List<Integer> waifus) {
        this.name = name;
        this.message = message;
        this.waifus = waifus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Integer> getWaifus() {
        return waifus;
    }

    public void setWaifus(List<Integer> waifus) {
        this.waifus = waifus;
    }
}
