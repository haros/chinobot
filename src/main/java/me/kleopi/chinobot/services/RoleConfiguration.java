package me.kleopi.chinobot.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RoleConfiguration extends Configuration {

    private final Map<String, String> assignableRoles;
    private final String inhouseRole;

    public static RoleConfiguration load() throws IOException {
        return gson.fromJson(loadString(), RoleConfiguration.class);
    }

    public RoleConfiguration(Map<String, String> assignableRoles, String inhouseRole) {
        this.assignableRoles = assignableRoles;
        this.inhouseRole = inhouseRole;
    }

    private RoleConfiguration() {
        this.assignableRoles = new HashMap<>();
        this.inhouseRole = null;
    }

    public Map<String, String> getAssignableRoles() {
        return assignableRoles;
    }

    public String getInhouseRole() {
        return inhouseRole;
    }
}
