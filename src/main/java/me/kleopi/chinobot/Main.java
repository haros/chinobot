package me.kleopi.chinobot;

import me.kleopi.chinobot.eventhandlers.CommandListener;
import me.kleopi.chinobot.services.BotConfiguration;
import me.kleopi.chinobot.services.Configuration;
import me.kleopi.chinobot.services.RoleConfiguration;
import me.kleopi.chinobot.services.WaifuConfiguration;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;

import javax.security.auth.login.LoginException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws LoginException, IOException {
        final BotConfiguration config = BotConfiguration.load();
        final RoleConfiguration roleConfig = RoleConfiguration.load();
        final WaifuConfiguration waifuConfig = WaifuConfiguration.load();

        if (config == null || roleConfig == null || waifuConfig == null) {
            System.out.println("Please fill in all the configuration files.");
            return;
        }

        JDABuilder.createDefault(config.getToken())
                .build()
                .addEventListener(new CommandListener(
                    BotConfiguration.load(),
                    RoleConfiguration.load(),
                    WaifuConfiguration.load())
                );

        System.out.println("Finished building JDA.");
    }
}